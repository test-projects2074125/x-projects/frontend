// Utils
import { createTheme, MantineProvider } from "@mantine/core";
import AppRouter from "@x-projects/routes/sections";

function App() {
  const theme = createTheme({});

  return (
    <MantineProvider theme={theme}>
      <AppRouter />
    </MantineProvider>
  );
}

export default App;
