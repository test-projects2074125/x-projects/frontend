import { create } from "zustand";
// Types
import type { IUserCardData } from "@x-projects/types/data/account/account.types.ts";

interface Store {
  loading: boolean;
  users: IUserCardData[] | null;
  setUsers: (users: IUserCardData[]) => void;
}

export const useAccountStore = create<Store>((set) => ({
  loading: true,
  users: null,
  setUsers: (users: IUserCardData[]) => {
    set(() => ({
      loading: false,
      users,
    }));
  },
}));
