import { create } from "zustand";
// Utils
import { setAuthAccessToken } from "@x-projects/api/fetcher/fetcher.ts";
// Types
import type { IUserData } from "@x-projects/types/data/auth/auth.types.ts";
import type { IAuthUserResData } from "@x-projects/types/data/auth/auth-api.types.ts";

interface Store {
  loading: boolean;
  authenticated: boolean;
  userData: IUserData | null;
  setAuth: (authData: IAuthUserResData) => void;
  setUserData: (userData: IAuthUserResData["user"]) => void;
  resetAuth: () => void;
}

export const useAuthStore = create<Store>((set, getState) => ({
  loading: true,
  authenticated: false,
  userData: null,
  setAuth: (authData: IAuthUserResData) => {
    setAuthAccessToken(authData.accessToken);
    set(() => ({
      loading: false,
      authenticated: true,
      userData: authData.user,
    }));
  },
  setUserData: (userData: IAuthUserResData["user"]) => {
    set(() => ({
      ...getState(),
      userData,
    }));
  },
  resetAuth: () => {
    setAuthAccessToken(null);
    set({
      loading: false,
      authenticated: false,
      userData: null,
    });
  },
}));
