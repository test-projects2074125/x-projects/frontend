import fetcher, { endpoints } from "@x-projects/api/fetcher/fetcher.ts";
// Types
import type {
  IAuthUserResData,
  ISignupResData,
} from "@x-projects/types/data/auth/auth-api.types.ts";
import type {
  IEditProfileResData,
  IListUsersResData,
} from "@x-projects/types/data/account/account-api.types.ts";

export class AccountRequests {
  private constructor() {}

  /** Auth **/
  public static async login(email: string, password: string): Promise<IAuthUserResData> {
    try {
      const res = await fetcher.post<IAuthUserResData>(endpoints.login, {
        email,
        password,
      });

      return res.data;
    } catch (exc) {
      throw exc;
    }
  }

  public static async checkAuth(): Promise<IAuthUserResData> {
    try {
      const res = await fetcher.post<IAuthUserResData>(endpoints.checkAuth);

      return res.data;
    } catch (exc) {
      throw exc;
    }
  }

  public static async signup(formData: FormData): Promise<ISignupResData> {
    try {
      const res = await fetcher.post<ISignupResData>(endpoints.signup, formData);

      return res.data;
    } catch (exc) {
      throw exc;
    }
  }

  /** Account **/
  public static async listUsers(): Promise<IListUsersResData> {
    try {
      const res = await fetcher.get<IListUsersResData>(endpoints.listUsers);

      return res.data;
    } catch (exc) {
      throw exc;
    }
  }

  public static async editProfile(formData: FormData): Promise<IEditProfileResData> {
    try {
      const res = await fetcher.put<IEditProfileResData>(endpoints.editAccount, formData);

      return res.data;
    } catch (exc) {
      throw exc;
    }
  }
}
