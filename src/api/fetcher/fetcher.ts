import axios from "axios";
// Utils
import { isNil } from "lodash";

const fetcher = axios.create({
  baseURL: import.meta.env.VITE_BASE_API_URL,
  headers: {
    Authorization: isNil(localStorage.getItem("accessToken"))
      ? ""
      : `Bearer ${localStorage.getItem("accessToken")}`,
  },
});

export default fetcher;

export const setAuthAccessToken = (accessToken: string | null): void => {
  if (accessToken === null) {
    fetcher.defaults.headers["Authorization"] = "";
    localStorage.removeItem("accessToken");
  } else {
    fetcher.defaults.headers["Authorization"] = `Bearer ${accessToken}`;
    localStorage.setItem("accessToken", accessToken);
  }
};

export const hasAccessToken = (): boolean => {
  return !isNil(localStorage.getItem("accessToken"));
};

export const serveUserImage = (imagePath: string | null): string | null => {
  if (isNil(imagePath)) {
    return null;
  }
  return `${import.meta.env.VITE_BASE_IMG_URL}${imagePath}`;
};

export const endpoints = {
  login: "v1/auth/login",
  checkAuth: "v1/auth/check",
  signup: "v1/auth/signup",
  //
  editAccount: `v1/account/edit`,
  listUsers: `v1/account/users`,
} as const;
