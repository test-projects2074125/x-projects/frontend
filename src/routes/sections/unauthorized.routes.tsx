import { Suspense } from "react";
import { Outlet } from "react-router-dom";
// Components
import AuthPage from "@x-projects/pages/auth/auth.page.tsx";
import GuestGuard from "@x-projects/guard/guest.guard.tsx";
import LoadingPage from "@x-projects/pages/loader/loader.page.tsx";

export const unauthorizedRoutes = {
  path: "/",
  element: (
    <GuestGuard>
      <Suspense fallback={<LoadingPage fullViewport />}>
        <Outlet />
      </Suspense>
    </GuestGuard>
  ),
  children: [{ element: <AuthPage />, index: true }],
};
