import { Navigate, useRoutes } from "react-router-dom";
// Routes
import { unauthorizedRoutes } from "@x-projects/routes/sections/unauthorized.routes.tsx";
import { authorizedRoutes } from "@x-projects/routes/sections/authorized.routes.tsx";

export default function AppRouter() {
  return useRoutes([
    // Unauthorized routes
    unauthorizedRoutes,

    // Authorized routes
    ...authorizedRoutes,

    // No match 404
    { path: "*", element: <Navigate to="/404" replace /> },
  ]);
}
