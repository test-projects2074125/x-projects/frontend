import { lazy, Suspense } from "react";
import { Outlet } from "react-router-dom";
// Utils
import { paths } from "@x-projects/routes/paths.ts";
// Components
import AuthGuard from "@x-projects/guard/auth.guard.tsx";
// Pages
import LoadingPage from "@x-projects/pages/loader/loader.page.tsx";

// ----------------------------------------------------------------------

const ListUsersPage = lazy(() => import("@x-projects/pages/list-users/list-users.page"));
const ProfilePage = lazy(() => import("@x-projects/pages/profile/profile.page"));

// ----------------------------------------------------------------------

export const authorizedRoutes = [
  {
    path: "/",
    element: (
      <AuthGuard>
        <Suspense fallback={<LoadingPage fullViewport />}>
          <Outlet />
        </Suspense>
      </AuthGuard>
    ),
    children: [
      { path: paths.people, element: <ListUsersPage /> },
      { path: paths.profile, element: <ProfilePage /> },
    ],
  },
];
