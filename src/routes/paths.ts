export const paths = {
  auth: "/",
  profile: "/account",
  people: "/people",
} as const;
