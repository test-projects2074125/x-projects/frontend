export type ApiFailureRes<T extends object = object> = T & {
  message: string;
  [key: string]: any;
};

export type ApiValidationErrorRes<T extends object> = ApiFailureRes & {
  errors: { [K in keyof T]?: string };
};
