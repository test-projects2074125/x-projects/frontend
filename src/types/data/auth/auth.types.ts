import { Sex } from "@x-projects/utils/constants/common.constants.ts";

export interface IUserData {
  id: string;
  name: string;
  email: string;
  sex: Sex;
  birthday: string;
  imagePath: string | null;
}
