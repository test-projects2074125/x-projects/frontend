import { IUserData } from "./auth.types.ts";

export interface IAuthUserResData {
  accessToken: string;
  user: IUserData;
}

export interface ISignupResData {
  success: boolean;
}
