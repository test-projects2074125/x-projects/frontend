export interface IUserCardData {
  id: string;
  name: string;
  birthday: string;
  imagePath: string | null;
}
