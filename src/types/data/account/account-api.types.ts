import { IUserCardData } from "@x-projects/types/data/account/account.types.ts";
import { IUserData } from "@x-projects/types/data/auth/auth.types.ts";

export interface IListUsersResData {
  total: number;
  rows: IUserCardData[];
}

export type IEditProfileResData = IUserData;
