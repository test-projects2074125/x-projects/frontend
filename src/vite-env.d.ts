/// <reference types="vite/client" />

declare global {
  interface ImportMetaEnv {
    readonly VITE_BASE_API_URL: string;
    readonly VITE_BASE_IMG_URL: string;
  }
}
