import ReactDOM from "react-dom/client";
import { Suspense } from "react";
import { BrowserRouter } from "react-router-dom";
import { HelmetProvider } from "react-helmet-async";

import { MantineProvider } from "@mantine/core";
import { Notifications } from "@mantine/notifications";

import App from "@x-projects/app.tsx";

import "@mantine/core/styles.css";
import "@mantine/notifications/styles.css";
import "./styles/index.scss";

ReactDOM.createRoot(document.getElementById("root")!).render(
  <MantineProvider>
    <Notifications />
    <HelmetProvider>
      <BrowserRouter>
        <Suspense>
          <App />
        </Suspense>
      </BrowserRouter>
    </HelmetProvider>
  </MantineProvider>,
);
