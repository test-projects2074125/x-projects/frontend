import { useEffect, useMemo, useState } from "react";
// Utils
import { useAuthStore } from "@x-projects/store/auth.store.ts";
import { handleRequest } from "@x-projects/utils/api.util.ts";
import { AccountRequests } from "@x-projects/api/requests/account.requests.ts";
import { hasAccessToken } from "@x-projects/api/fetcher/fetcher.ts";
// Components
import LoginForm from "@x-projects/pages/auth/components/login-form.tsx";
import RegistrationForm from "@x-projects/pages/auth/components/registration-form.tsx";
import LoadingPage from "@x-projects/pages/loader/loader.page.tsx";
import { Helmet } from "react-helmet-async";
import { Container, Flex } from "@mantine/core";

function AuthPage() {
  const authStore = useAuthStore();
  //
  const [isLoginForm, setIsLoginForm] = useState<boolean>(true);

  useEffect(() => {
    if (hasAccessToken()) {
      handleRequest(AccountRequests.checkAuth, {
        success: authStore.setAuth,
        failure: authStore.resetAuth,
        exception: authStore.resetAuth,
      });
    } else {
      authStore.resetAuth();
    }
  }, []);

  const content = useMemo(() => {
    if (authStore.loading) {
      return <LoadingPage />;
    }
    if (isLoginForm) {
      return <LoginForm switchToRegistrationForm={() => setIsLoginForm(false)} />;
    }
    return <RegistrationForm switchToLoginForm={() => setIsLoginForm(true)} />;
  }, [authStore.loading, isLoginForm]);

  return (
    <>
      <Helmet title="Авторизация" />
      <Container size="xs">
        <Flex mih="100vh" justify="center" direction="column" py={40}>
          {content}
        </Flex>
      </Container>
    </>
  );
}

export default AuthPage;
