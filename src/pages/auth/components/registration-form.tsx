import React, { useCallback, useState } from "react";
// Utils
import * as _ from "lodash";
import { isEmail } from "validator";
import { AccountRequests } from "@x-projects/api/requests/account.requests.ts";
import { notifications } from "@mantine/notifications";
import { handleRequest } from "@x-projects/utils/api.util.ts";
// Components
import {
  TextInput,
  Text,
  Button,
  SegmentedControl,
  Grid,
  Center,
  Avatar,
  FileInput,
  Flex,
  Box,
} from "@mantine/core";
// Types and Enums
import { Sex } from "@x-projects/utils/constants/common.constants.ts";
import type { ISignupResData } from "@x-projects/types/data/auth/auth-api.types.ts";
import type { ApiValidationErrorRes } from "@x-projects/types/common.types.ts";

// ----------------------------------------------------------------------

interface RegistrationFormData {
  name: string;
  email: string;
  password: string;
  birthday: string;
  sex: Sex;
  image: File | null;
  imagePreview: string | ArrayBuffer | null;
}

const formErrorsInitial = {
  name: null,
  email: null,
  password: null,
  birthday: null,
  sex: null,
  image: null,
  imagePreview: null,
};

type Props = {
  switchToLoginForm: VoidFunction;
};

function RegistrationForm({ switchToLoginForm }: Props) {
  const [formData, setFormData] = useState<RegistrationFormData>({
    name: "",
    email: "",
    password: "",
    birthday: "",
    sex: Sex.Male,
    image: null,
    imagePreview: null,
  });
  const [formErrors, setFormErrors] =
    useState<Record<keyof RegistrationFormData, string | null>>(formErrorsInitial);
  const [loading, setLoading] = useState<boolean>(false);
  //
  const fileInputElement = React.useRef<HTMLButtonElement>();

  const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const { name, value } = event.target;
    //
    if (_.eq(name, "birthday")) {
      const birthday = new Date(value);

      if (new Date().getFullYear() - birthday.getFullYear() < 16) {
        return setFormErrors({ ...formErrors, birthday: "Минимальный допустимый возраст 16" });
      }
      setFormErrors({ ...formErrorsInitial });
    }
    setFormData({ ...formData, [name]: value });
  };

  const handleSelectChange = (value: Sex) => {
    setFormData({ ...formData, sex: value });
  };

  const handleImageChange = (file: File | null) => {
    if (file) {
      const reader = new FileReader();
      //
      reader.onloadend = () => {
        if (reader.result) {
          setFormData({ ...formData, image: file, imagePreview: reader.result });
        }
      };
      reader.readAsDataURL(file);
    }
  };

  const validateForm = useCallback((): boolean => {
    const errors: Partial<typeof formErrors> = {};

    if (!_.isString(formData.name) || formData.name.length < 3) {
      errors.name = "Введите ваше имя";
    }
    if (!_.isString(formData.email) || !isEmail(formData.email)) {
      errors.email = "Введите корректный эл. адрес";
    }
    if (!_.isString(formData.password) || formData.password.length < 6) {
      errors.password = "Должен содержать не менее 6 символов";
    }
    if (!_.isString(formData.birthday) || isNaN(new Date(formData.birthday).getTime())) {
      errors.birthday = "Выберите дату рождения";
    }
    const isValid = _.isEmpty(errors);

    if (isValid) {
      setFormErrors(formErrorsInitial);
    } else {
      setFormErrors({ ...formErrorsInitial, ...errors });
    }
    return isValid;
  }, [formData, setFormErrors]);

  const handleSubmit = useCallback(() => {
    const _formData = new FormData();

    _formData.append("name", formData.name);
    _formData.append("email", formData.email);
    _formData.append("password", formData.password);
    _formData.append("birthday", new Date(formData.birthday).toISOString());
    _formData.append("sex", formData.sex);

    if (!_.isNil(formData.image)) {
      _formData.append("image", formData.image);
    }
    //
    handleRequest<ISignupResData, ApiValidationErrorRes<RegistrationFormData>>(
      () => {
        setLoading(true);
        //
        return AccountRequests.signup(_formData);
      },
      {
        success: (data) => {
          if (data.success) {
            switchToLoginForm();
            //
            notifications.show({
              message: "Аккаунт создан успешно",
              color: "green",
              autoClose: true,
            });
          } else {
            // Unexpected behavior
            notifications.show({
              title: "Ошибка",
              message: "Упс! при регистрации произошла неожиданная ошибка",
            });
          }
        },
        failure: (err) => {
          if (_.isObject(err.response?.data.errors) && !_.isEmpty(err.response?.data.errors)) {
            setFormErrors({ ...formErrorsInitial, ...err.response.data.errors });
          } else {
            notifications.show({
              title: "Ошибка",
              message: err.response?.data.message ?? "Произошла неожиданная ошибка",
              color: "red",
              autoClose: true,
            });
          }
        },
        finally: () => setLoading(false),
      },
    );
  }, [formData, setFormErrors]);

  return (
    <>
      <Text fw="bold" size="xl" ta="center" my="20px">
        Регистрация
      </Text>
      <form
        onSubmit={(e) => {
          e.preventDefault();
          //
          const isValid = validateForm();

          if (isValid) {
            handleSubmit();
          }
        }}
      >
        <Flex align="center" direction="column">
          <Text size="14px">Изображение профиля</Text>
          <Avatar
            src={_.isString(formData.imagePreview) ? formData.imagePreview : null}
            alt="Avatar"
            radius="50%"
            size={120}
            mt="10px"
            mb="20px"
            style={{ cursor: "pointer" }}
            onClick={() => fileInputElement.current?.click()}
          />
          <FileInput
            name="image"
            label="Изображение профиля"
            placeholder="Загрузите ваше изображение"
            accept="image/*"
            ref={fileInputElement as any}
            onChange={handleImageChange}
            style={{ display: "none" }}
          />
          <Box w="125px">
            <Text ta="center">Пол</Text>
            <SegmentedControl
              value={formData.sex}
              onChange={(val) => handleSelectChange(val as Sex)}
              data={[
                { value: Sex.Male, label: "М" },
                { value: Sex.Female, label: "Ж" },
              ]}
              fullWidth
              mt="4px"
            />
          </Box>
        </Flex>
        <Grid justify="center" align="center" mt="md" gutter="md">
          <Grid.Col span={6} style={{ justifyContent: "center", alignItems: "center" }}>
            <TextInput
              h="80px"
              label="Имя"
              placeholder="Введите ваше имя"
              name="name"
              value={formData.name}
              error={formErrors.name}
              onChange={handleInputChange}
              withAsterisk
            />
            <TextInput
              h="80px"
              label="Email"
              placeholder="Введите ваш email"
              name="email"
              value={formData.email}
              error={formErrors.email}
              onChange={handleInputChange}
              withAsterisk
              mt="sm"
            />
          </Grid.Col>
          <Grid.Col span={6} style={{ justifyContent: "center", alignItems: "center" }}>
            <TextInput
              h="80px"
              label="Пароль"
              placeholder="Введите ваш пароль"
              name="password"
              type="password"
              value={formData.password}
              error={formErrors.password}
              onChange={handleInputChange}
              withAsterisk
            />
            <TextInput
              h="80px"
              label="Дата рождения"
              placeholder="Введите дату вашего рождения"
              name="birthday"
              type="date"
              value={formData.birthday}
              error={formErrors.birthday}
              onChange={handleInputChange}
              withAsterisk
              mt="sm"
            />
          </Grid.Col>
        </Grid>
        <Center mt="md">
          <Button
            type="submit"
            variant="outline"
            mt="20px"
            px="xl"
            disabled={loading}
            loading={loading}
          >
            Зарегистрироваться
          </Button>
        </Center>
        <Center mt="xs">
          <Text mr={6}>Уже зарегистрированы?</Text>
          <Text onClick={switchToLoginForm} c="blue" fw="bold" style={{ cursor: "pointer" }}>
            Войти
          </Text>
        </Center>
      </form>
    </>
  );
}

export default RegistrationForm;
