import React, { useCallback, useState } from "react";
// Utils
import * as _ from "lodash";
import { useAuthStore } from "@x-projects/store/auth.store.ts";
import { notifications } from "@mantine/notifications";
import { useRouter } from "@x-projects/routes/hooks/use-router.ts";
import { handleRequest } from "@x-projects/utils/api.util.ts";
import { isEmail } from "validator";
import { AccountRequests } from "@x-projects/api/requests/account.requests.ts";
import { paths } from "@x-projects/routes/paths.ts";
// Components
import { TextInput, Text, Button, Center } from "@mantine/core";
// Types
import type { IAuthUserResData } from "@x-projects/types/data/auth/auth-api.types.ts";
import type { ApiValidationErrorRes } from "@x-projects/types/common.types.ts";

// ----------------------------------------------------------------------

interface LoginFormData {
  email: string;
  password: string;
}

const formErrorsInitial = {
  email: null,
  password: null,
};

type Props = {
  switchToRegistrationForm: VoidFunction;
};

function LoginForm({ switchToRegistrationForm }: Props) {
  const authStore = useAuthStore();
  const router = useRouter();
  //
  const [formData, setFormData] = useState<LoginFormData>({
    email: "",
    password: "",
  });
  const [formErrors, setFormErrors] =
    useState<Record<keyof LoginFormData, string | null>>(formErrorsInitial);
  const [loading, setLoading] = useState<boolean>(false);

  const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const { name, value } = event.target;
    setFormData({ ...formData, [name]: value });
  };

  const validateForm = useCallback((): boolean => {
    const errors: Partial<typeof formErrors> = {};

    if (!_.isString(formData.email) || !isEmail(formData.email)) {
      errors.email = "Введите корректный эл. адрес";
    }
    if (!_.isString(formData.password) || formData.password.length < 6) {
      errors.password = "Введите корректный пароль";
    }
    const isValid = _.isEmpty(errors);

    if (isValid) {
      setFormErrors(formErrorsInitial);
    } else {
      setFormErrors({ ...formErrorsInitial, ...errors });
    }
    return isValid;
  }, [formData, setFormErrors]);

  const handleSubmit = useCallback(() => {
    handleRequest<IAuthUserResData, ApiValidationErrorRes<LoginFormData>>(
      () => {
        setLoading(true);
        //
        return AccountRequests.login(formData.email, formData.password);
      },
      {
        success: (data) => {
          setFormData({
            email: "",
            password: "",
          });
          authStore.setAuth(data);
          router.push(paths.people);
        },
        failure: (err) => {
          if (_.isObject(err.response?.data.errors) && !_.isEmpty(err.response?.data.errors)) {
            setFormErrors({ ...formErrorsInitial, ...err.response.data.errors });
          } else {
            notifications.show({
              title: "Ошибка",
              message: err.response?.data.message ?? "Произошла неожиданная ошибка",
              color: "red",
              autoClose: true,
            });
          }
        },
        finally: () => setLoading(false),
      },
    );
  }, [formData, setFormData]);

  return (
    <>
      <Text fw="bold" size="xl" ta="center" my="20px">
        Войти
      </Text>
      <form
        onSubmit={(e) => {
          e.preventDefault();
          //
          const isValid = validateForm();

          if (isValid) {
            handleSubmit();
          }
        }}
      >
        <TextInput
          label="Email"
          placeholder="Введите ваш email"
          name="email"
          type="email"
          value={formData.email}
          error={formErrors.email}
          onChange={handleInputChange}
        />
        <TextInput
          label="Пароль"
          placeholder="Введите ваш пароль"
          name="password"
          type="password"
          value={formData.password}
          error={formErrors.password}
          onChange={handleInputChange}
          mt="sm"
        />
        <Center mt="md">
          <Button
            type="submit"
            variant="outline"
            mt="20px"
            px="xl"
            disabled={loading}
            loading={loading}
          >
            Войти
          </Button>
        </Center>
        <Center mt="xs">
          <Text mr={6}>Нет аккаунта?</Text>
          <Text onClick={switchToRegistrationForm} c="blue" fw="bold" style={{ cursor: "pointer" }}>
            Зарегистрироваться
          </Text>
        </Center>
      </form>
    </>
  );
}

export default LoginForm;
