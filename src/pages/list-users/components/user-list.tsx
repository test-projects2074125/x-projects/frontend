// Utils
import { isEmpty } from "lodash";
import { serveUserImage } from "@x-projects/api/fetcher/fetcher.ts";
import { parsePlural } from "@x-projects/utils/common.util.ts";
// Components
import { Avatar, Box, Flex, Group, Text } from "@mantine/core";
// Types
import type { IUserCardData } from "@x-projects/types/data/account/account.types.ts";

type Props = {
  users: IUserCardData[];
};

function UserList({ users }: Props) {
  if (isEmpty(users)) {
    return (
      <Box my={20}>
        <Text>Список пользователей пуст</Text>
      </Box>
    );
  }
  return (
    <Flex direction="column" gap={10} my={20}>
      {users.map((user) => (
        <Group
          key={user.id}
          align="center"
          style={{ border: "1px solid rgba(0,0,0,.1)", borderRadius: "8px" }}
          p={10}
        >
          <Avatar src={serveUserImage(user.imagePath)} size={50} />
          <Box>
            <Text>{user.name}</Text>
            <Text>
              {parsePlural(new Date().getFullYear() - new Date(user.birthday).getFullYear(), {
                one: "год",
                few: "года",
                many: "лет",
              })}
            </Text>
          </Box>
        </Group>
      ))}
    </Flex>
  );
}

export default UserList;
