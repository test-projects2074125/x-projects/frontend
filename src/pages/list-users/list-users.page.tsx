import { useEffect, useMemo } from "react";
// Utils
import { isNil } from "lodash";
import { handleRequest } from "@x-projects/utils/api.util.ts";
import { AccountRequests } from "@x-projects/api/requests/account.requests.ts";
import { useAccountStore } from "@x-projects/store/account.store.ts";
import { notifications } from "@mantine/notifications";
// Components
import { Helmet } from "react-helmet-async";
import { Container, Text } from "@mantine/core";
import Header from "@x-projects/components/header/header.tsx";
import LoadingPage from "@x-projects/pages/loader/loader.page.tsx";
import UserList from "@x-projects/pages/list-users/components/user-list.tsx";

function ListUsersPage() {
  const accountStore = useAccountStore();
  //
  useEffect(() => {
    if (!isNil(accountStore.users)) {
      return;
    }
    handleRequest(AccountRequests.listUsers, {
      success: (data) => accountStore.setUsers(data.rows),
      failure: (error) => {
        notifications.show({
          title: "Ошибка",
          message: error.message,
          autoClose: true,
        });
      },
      exception: (error) => {
        notifications.show({
          title: "Ошибка",
          message: error.message,
          autoClose: true,
        });
      },
    });
  }, []);

  const content = useMemo(() => {
    if (accountStore.loading) {
      return <LoadingPage />;
    }
    if (isNil(accountStore.users)) {
      return "Ошибка";
    }
    return <UserList users={accountStore.users!} />;
  }, [accountStore.loading, accountStore.users]);

  return (
    <>
      <Helmet title="Пользователи" />
      <Header />
      <Container size="sm" py={40}>
        <Text size="lg" fw="bold" ta="center">
          Все пользователи
        </Text>
        {content}
      </Container>
    </>
  );
}

export default ListUsersPage;
