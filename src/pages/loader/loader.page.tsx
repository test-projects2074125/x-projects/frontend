import { Box, Loader } from "@mantine/core";

type Props = {
  size?: number;
  fullViewport?: boolean;
};

export default function LoadingPage({ size = 50, fullViewport = false }: Props) {
  return (
    <Box w={`100${fullViewport ? "vw" : "%"}`} h={`100${fullViewport ? "vh" : "%"}`}>
      <Loader
        size={size}
        type="dots"
        style={{
          position: "absolute",
          top: "50%",
          left: "50%",
          transform: "translate(-50%,-50%)",
        }}
      />
    </Box>
  );
}
