// Components
import { Helmet } from "react-helmet-async";
import Header from "@x-projects/components/header/header.tsx";
import EditProfileForm from "@x-projects/pages/profile/components/edit-profile-form.tsx";

function ProfilePage() {
  return (
    <>
      <Helmet title="Профиль" />
      <Header />
      <EditProfileForm />
    </>
  );
}

export default ProfilePage;
