import React, { useCallback, useMemo, useState } from "react";
// Utils
import * as _ from "lodash";
import { handleRequest } from "@x-projects/utils/api.util.ts";
import { AccountRequests } from "@x-projects/api/requests/account.requests.ts";
import { notifications } from "@mantine/notifications";
import { useAuthStore } from "@x-projects/store/auth.store.ts";
import { serveUserImage } from "@x-projects/api/fetcher/fetcher.ts";
// Components
import {
  Avatar,
  Box,
  Button,
  Center,
  Container,
  FileInput,
  Flex,
  Grid,
  SegmentedControl,
  Text,
  TextInput,
} from "@mantine/core";
// Types and Enums
import type { ApiValidationErrorRes } from "@x-projects/types/common.types.ts";
import type { IEditProfileResData } from "@x-projects/types/data/account/account-api.types.ts";
import { Sex } from "@x-projects/utils/constants/common.constants.ts";

// ----------------------------------------------------------------------

interface EditProfileFormData {
  name: string;
  password: string;
  image: File | null;
  imagePreview: string | ArrayBuffer | null;
}

const formErrorsInitial = {
  name: null,
  password: null,
  image: null,
  imagePreview: null,
};

function EditProfileForm() {
  const { userData, setUserData } = useAuthStore();
  //
  const [formData, setFormData] = useState<EditProfileFormData>({
    name: userData!.name,
    password: "",
    image: null,
    imagePreview: serveUserImage(userData!.imagePath),
  });
  const [formErrors, setFormErrors] =
    useState<Record<keyof EditProfileFormData, string | null>>(formErrorsInitial);
  const [loading, setLoading] = useState<boolean>(false);
  //
  const fileInputElement = React.useRef<HTMLButtonElement>();

  const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const { name, value } = event.target;
    //
    setFormData({ ...formData, [name]: value });
  };

  const handleImageChange = (file: File | null) => {
    if (file) {
      const reader = new FileReader();
      //
      reader.onloadend = () => {
        if (reader.result) {
          setFormData({ ...formData, image: file, imagePreview: reader.result });
        }
      };
      reader.readAsDataURL(file);
    }
  };

  const parsedBirthdayForInput = useMemo(() => {
    const date = new Date(userData!.birthday);
    const month = date.getMonth().toString();
    const day = date.getDate().toString();
    //
    return `${date.getFullYear()}-${month.length === 1 ? `0${month}` : month}-${day.length === 1 ? `0${day}` : day}`;
  }, [userData?.birthday]);

  const validateForm = useCallback((): boolean => {
    const errors: Partial<typeof formErrors> = {};

    if (!_.isString(formData.name) || formData.name.length < 3) {
      errors.name = "Введите ваше имя";
    }
    if (!_.isEmpty(formData.password) && formData.password.length < 6) {
      errors.password = "Должен содержать не менее 6 символов";
    }
    const isValid = _.isEmpty(errors);

    if (isValid) {
      setFormErrors(formErrorsInitial);
    } else {
      setFormErrors({ ...formErrorsInitial, ...errors });
    }
    return isValid;
  }, [formData, setFormErrors]);

  const handleSubmit = useCallback(() => {
    const _formData = new FormData();

    if (!_.isEmpty(formData.name)) {
      _formData.append("name", formData.name);
    }
    if (!_.isEmpty(formData.password)) {
      _formData.append("password", formData.password);
    }
    if (!_.isNil(formData.image)) {
      _formData.append("image", formData.image);
    }
    //
    handleRequest<IEditProfileResData, ApiValidationErrorRes<EditProfileFormData>>(
      () => {
        setLoading(true);
        //
        return AccountRequests.editProfile(_formData);
      },
      {
        success: (data) => {
          notifications.show({
            message: "Изменения сохранены",
            color: "green",
            autoClose: true,
          });
          setUserData(data);
        },
        failure: (err) => {
          if (_.isObject(err.response?.data.errors) && !_.isEmpty(err.response?.data.errors)) {
            setFormErrors({ ...formErrorsInitial, ...err.response.data.errors });
          } else {
            notifications.show({
              title: "Ошибка",
              message: err.response?.data.message ?? "Произошла неожиданная ошибка",
              color: "red",
              autoClose: true,
            });
          }
        },
        finally: () => setLoading(false),
      },
    );
  }, [formData, setFormErrors]);

  return (
    <Container size="sm" py={40}>
      <Text size="lg" fw="bold" ta="center" mb="md">
        Профиль
      </Text>
      <form
        onSubmit={(e) => {
          e.preventDefault();
          //
          const isValid = validateForm();

          if (isValid) {
            handleSubmit();
          }
        }}
      >
        <Flex align="center" direction="column">
          <Text size="14px">Изображение профиля</Text>
          <Avatar
            src={_.isString(formData.imagePreview) ? formData.imagePreview : null}
            alt="Avatar"
            radius="50%"
            size={120}
            mt="10px"
            mb="20px"
            style={{ cursor: "pointer" }}
            onClick={() => fileInputElement.current?.click()}
          />
          <FileInput
            name="image"
            label="Изображение профиля"
            placeholder="Загрузите ваше изображение"
            accept="image/*"
            ref={fileInputElement as any}
            onChange={handleImageChange}
            style={{ display: "none" }}
          />
          <Box w="125px">
            <Text ta="center">Пол</Text>
            <SegmentedControl
              value={userData!.sex}
              data={[
                { value: Sex.Male, label: "М" },
                { value: Sex.Female, label: "Ж" },
              ]}
              disabled
              fullWidth
              mt="4px"
            />
          </Box>
        </Flex>
        <Grid justify="center" align="center" mt="md" gutter="md">
          <Grid.Col span={6} style={{ justifyContent: "center", alignItems: "center" }}>
            <TextInput
              h="80px"
              label="Имя"
              placeholder="Введите ваше имя"
              name="name"
              value={formData.name}
              onChange={handleInputChange}
            />
            <TextInput
              h="80px"
              label="Email"
              placeholder="Введите ваш email"
              name="email"
              value={userData!.email}
              onChange={handleInputChange}
              disabled
              mt="sm"
            />
          </Grid.Col>
          <Grid.Col span={6} style={{ justifyContent: "center", alignItems: "center" }}>
            <TextInput
              h="80px"
              label="Новый пароль"
              placeholder="Новый пароль"
              name="password"
              type="password"
              value={formData.password}
              error={formErrors.password}
              onChange={handleInputChange}
            />
            <TextInput
              h="80px"
              label="Дата рождения"
              placeholder="Введите дату вашего рождения"
              name="birthday"
              type="date"
              value={parsedBirthdayForInput}
              disabled
              mt="sm"
            />
          </Grid.Col>
        </Grid>
        <Center mt="md">
          <Button
            type="submit"
            variant="outline"
            mt="20px"
            px="xl"
            disabled={loading}
            loading={loading}
          >
            Сохранить
          </Button>
        </Center>
      </form>
    </Container>
  );
}

export default EditProfileForm;
