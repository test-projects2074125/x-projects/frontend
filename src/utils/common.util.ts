type ParsePluralOptions = {
  one: string;
  few: string;
  many: string;
};

export function parsePlural(num: number, options: ParsePluralOptions): string {
  const numberString = num.toString();
  const lastDigit = Number.parseInt(numberString.at(numberString.length - 1)!);

  if (num >= 10 && num <= 20) {
    return `${numberString} ${options.many}`;
  }
  if (num > 100) {
    const penultimateDigit: number = Number.parseInt(
      numberString.substring(numberString.length - 2),
    );

    if (penultimateDigit >= 10 && penultimateDigit <= 20) {
      return `${numberString} ${options.many}`;
    }
  }
  if (lastDigit === 1) {
    return `${numberString} ${options.one}`;
  }
  if (lastDigit > 1 && lastDigit < 5) {
    return `${numberString} ${options.few}`;
  }
  return `${numberString} ${options.many}`;
}
