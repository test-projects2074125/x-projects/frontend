import { AxiosError } from "axios";

type RequestCallbacks<T, F = { message: string }, E = Error> = {
  success: (data: T) => void;
  failure?: (failure: AxiosError<F>) => void;
  exception?: (failure: E) => void;
  finally?: () => void;
};

export function handleRequest<T, F = { message: string }, E = Error>(
  request: () => Promise<T>,
  callbacks: RequestCallbacks<T, F, E>,
): void {
  request()
    .then(callbacks.success)
    .catch((exc) => {
      if (exc instanceof AxiosError && callbacks.failure) {
        callbacks.failure(exc);
      } else if (callbacks.exception) {
        callbacks.exception(exc);
      }
    })
    .finally(callbacks.finally);
}
