import React from "react";
// Utils
import { useAuthStore } from "@x-projects/store/auth.store.ts";
import { paths } from "@x-projects/routes/paths.ts";
// Components
import { Navigate, useLocation } from "react-router-dom";

// ----------------------------------------------------------------------

type Props = {
  children: React.ReactNode;
};

export default function AuthGuard({ children }: Props) {
  const { authenticated } = useAuthStore();
  const location = useLocation();

  if (!authenticated) {
    return <Navigate to={`${paths.auth}?navigateTo=${location.pathname}`} replace />;
  }
  return <>{children}</>;
}
