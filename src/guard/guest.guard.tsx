import React from "react";
// Utils
import { isEmpty, isString } from "lodash";
import { useAuthStore } from "@x-projects/store/auth.store.ts";
import { paths } from "@x-projects/routes/paths.ts";
// Components
import { Navigate, useSearchParams } from "react-router-dom";

// ----------------------------------------------------------------------

type Props = {
  children: React.ReactNode;
};

export default function GuestGuard({ children }: Props) {
  const { authenticated } = useAuthStore();
  const [searchParams] = useSearchParams();

  if (authenticated) {
    const navigateTo = searchParams.get("navigateTo");

    if (isString(navigateTo) && !isEmpty(navigateTo)) {
      return <Navigate to={navigateTo} replace />;
    }
    return <Navigate to={paths.people} replace />;
  }
  return <>{children}</>;
}
