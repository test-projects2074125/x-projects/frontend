// Utils
import { useRouter } from "@x-projects/routes/hooks/use-router.ts";
import { setAuthAccessToken } from "@x-projects/api/fetcher/fetcher.ts";
import { paths } from "@x-projects/routes/paths.ts";
import { useAuthStore } from "@x-projects/store/auth.store.ts";
// Components
import { Link, useLocation } from "react-router-dom";
import { Anchor, Button, Container, Flex, Group, Text } from "@mantine/core";

function Header() {
  const authStore = useAuthStore();
  const router = useRouter();
  const location = useLocation();

  const onNavigateToHomePage = () => {
    if (location.pathname.startsWith(paths.people)) {
      return;
    }
    router.push(paths.people);
  };

  const onLogout = () => {
    setAuthAccessToken(null);
    authStore.resetAuth();
  };

  return (
    <Container size="sm" my={10}>
      <Flex justify="space-between" align="center">
        <Text fw="bold" style={{ cursor: "pointer" }} onClick={onNavigateToHomePage}>
          X-Projects
        </Text>
        <Group>
          <Anchor component={Link} to={paths.profile} size="sm" style={{ textDecoration: "none" }}>
            Профиль
          </Anchor>
          <Button variant="white" onClick={onLogout}>
            Выйти
          </Button>
        </Group>
      </Flex>
    </Container>
  );
}

export default Header;
