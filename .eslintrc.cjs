module.exports = {
  root: true,
  parser: "@typescript-eslint/parser",
  plugins: ["prettier", "@typescript-eslint/eslint-plugin", "react-refresh"],
  extends: ["prettier", "plugin:@typescript-eslint/recommended", "plugin:react-hooks/recommended"],
  env: {
    browser: true,
    es2020: true,
  },
  ignorePatterns: [".eslintrc.js", "dist", "test", "webpack.config.js"],
  overrides: [
    {
      files: ["*.ts", "*.tsx"],
      extends: ["prettier", "plugin:@typescript-eslint/recommended"],
      parserOptions: {
        project: ['./tsconfig.app.json'],
      },
      rules: {
        "@typescript-eslint/recommended": [
          "off",
          {
            enforceBuildableLibDependency: true,
            allow: [],
            depConstraints: [
              {
                sourceTag: "*",
                onlyDependOnLibsWithTags: ["*"]
              }
            ]
          }
        ],
        "react-hooks/exhaustive-deps": "off",
        "@typescript-eslint/interface-name-prefix": "off",
        "@typescript-eslint/explicit-function-return-type": "off",
        "@typescript-eslint/explicit-module-boundary-types": "off",
        "@typescript-eslint/no-explicit-any": "off",
        "@typescript-eslint/no-var-requires": "off",
        "@typescript-eslint/no-empty-interface": "off",
        "@typescript-eslint/no-non-null-assertion": "off",
        "@typescript-eslint/no-namespace": "off",
        "@typescript-eslint/no-extra-non-null-assertion": "error",
        "@typescript-eslint/no-confusing-non-null-assertion": "error",
        "@typescript-eslint/non-nullable-type-assertion-style": "error",
        "@typescript-eslint/consistent-type-assertions": ["error", { assertionStyle: 'as', objectLiteralTypeAssertions: 'allow-as-parameter' }],
        "@typescript-eslint/explicit-member-accessibility": ["error", { overrides: { constructors: "no-public" } }],
        "@typescript-eslint/no-unused-vars": ["error", { "vars": "all", "args": "none", "ignoreRestSiblings": true }],
        "no-async-promise-executor": 0
      },
    },
  ],
  rules: {
    "prettier/prettier": "error",
    "lines-between-class-members": ["error", "always", { "exceptAfterSingleLine": true }],
    semi: "error",
    "no-var": "error",
    curly: "error",
    eqeqeq: "warn",
    quotes: ["error", "double"],
    "max-len": [0, 100, 2, { ignoreUrls: true }],
    "no-unused-vars": ["error", { "vars": "all", "args": "none", "ignoreRestSiblings": false }],
    "object-curly-spacing": ["error", "always"],
    "template-curly-spacing": ["error", "always"],
    'react-refresh/only-export-components': [
      'off',
      { allowConstantExport: true },
    ],
  },
};
